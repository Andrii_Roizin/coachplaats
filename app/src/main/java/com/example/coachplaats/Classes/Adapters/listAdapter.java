package com.example.coachplaats.Classes.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.coachplaats.Classes.Matching;
import com.example.coachplaats.R;

import java.util.ArrayList;

public class listAdapter extends CursorAdapter {

    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;
    private ArrayList<Matching> matchings;

    public listAdapter(Context context, Cursor c, ArrayList<Matching> matchings) {
        super(context, c, 0);
        this.matchings = matchings;
        myDbHelper = new myDbHelper(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.coaches_list, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ImageView imageView = view.findViewById(R.id.photo_image);

        TextView nameView = view.findViewById(R.id.coach_first_name);
        TextView date_of_birth_view = view.findViewById(R.id.prefix);
        TextView availability_view = view.findViewById(R.id.coach_availability);
        TextView city_view = view.findViewById(R.id.coach_city);

        nameView.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.FIRST_NAME)));
        city_view.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.CITY)));
        availability_view.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.AVAILABILITY)));
        date_of_birth_view.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.DATE_OF_BIRTH)));

        imageView.setImageBitmap(BitmapFactory.decodeFile(cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO))));

    }
}
