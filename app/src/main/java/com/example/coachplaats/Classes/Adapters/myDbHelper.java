package com.example.coachplaats.Classes.Adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class myDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Coachplaats";    // Database Name
    private static final int DATABASE_Version = 1;   // Database Version
    public static final String TABLE_COACH = "Coach";// Table Name
    public static final String TABLE_CLIENT = "Client";
    public static final String TABLE_ADMIN = "Admin";
    public static final String ID = "_id";     // Column I (Primary Key)
    public static final String FIRST_NAME = "Name";   //Column II
    public static final String PREFIX = "PREFIX";   //Added
    public static final String SURNAME = "SURNAME"; //Added
    public static final String GENDER = "GENDER";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String EMAIL = "EMAIL";
    public static final String PASSWORD = "PASSWORD";
    public static final String VERIFIED = "VERIFIED";
    public static final String COMPANY_KVK = "COMPANY_KVK";
    public static final String COMPANY_NAME = "COMPANY_NAME";
    public static final String COMPANY_TYPE = "COMPANY_TYPE";
    public static final String DATE_OF_BIRTH = "DATE_OF_BIRTH";
    public static final String POSITION_IN_COMPANY = "POSITION_IN_COMPANY";
    public static final String COMPANY_ADDRESS = "COMPANY_ADDRESS";
    public static final String ZIPCODE = "ZIPCODE";
    public static final String CITY = "CITY";
    public static final String VAT_NUMBER = "VAT_NUMBER";
    public static final String BANK_ACCOUNT = "BANK_ACCOUNT";
    public static final String LANGUAGES = "LANGUAGES";
    public static final String AVAILABILITY = "AVAILABILITY";
    public static final String COACH_BIO = "COACH_BIO";
    public static final String ADDRESS = "ADDRESS";
    public static final String PHOTO = "PHOTO";
    public static final String DOC_PHOTO = "DOC_PHOTO";
    public static final String INDIVIDUAL_COACHING = "INDIVIDUAL_COACHING";
    public static final String CAREER_OUTPLACEMENT = "CAREER_OUTPLACEMENT";
    public static final String INTERVISION_COACHING = "INTERVISION_COACHING";
    public static final String TEAM_COACHING = "TEAM_COACHING";
    public static final String LEAN_COACHING = "LEAN_COACHING";
    public static final String AGILE_COACHING = "AGILE_COACHING";
    public static final String EXECUTIVE_COACHING = "EXECUTIVE_COACHING";
    public static final String HAPPINESS_AT_WORK = "HAPPINESS_AT_WORK";
    public static final String PROJECT_MANAGERS = "PROJECT_MANAGERS";
    public static final String ADVISIORS = "ADVISIORS";
    public static final String MBO = "MBO";
    public static final String HPO = "HPO";
    public static final String P_HBO = "P_HBO";
    public static final String ACADEMIC = "ACADEMIC";
    public static final String OTHER = "OTHER";
    public static final String ANSWER_1 = "ANSWER_1";
    public static final String ANSWER_2 = "ANSWER_2";
    public static final String ANSWER_3 = "ANSWER_3";
    public static final String ANSWER_4 = "ANSWER_4";
    public static final String ANSWER_5 = "ANSWER_5";
    public static final String ANSWER_6 = "ANSWER_6";
    public static final String ANSWER_7 = "ANSWER_7";
    public static final String ANSWER_8 = "ANSWER_8";
    public static final String SHORT_BIO = "SHORT_BIO";
    public static final String LONG_BIO = "LONG_BIO";
    public static final String YES_NO_1 = "YES_NO_1";
    public static final String YES_NO_2 = "YES_NO_2";
    public static final String YES_NO_3 = "YES_NO_3";





    private static final String CREATE_COACH = "CREATE TABLE " + TABLE_COACH + " ("
            //Registration
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FIRST_NAME + " VARCHAR(255), "
            + PREFIX + " VARCHAR(255), "
            + SURNAME+ " VARCHAR(255), "
            + EMAIL + " VARCHAR(255),"
            + PASSWORD + " VARCHAR(255), "
            + PHONE_NUMBER + " VARCHAR(255),"

            //Rest fields
            + DATE_OF_BIRTH + " VARCHAR(255), "
            + COMPANY_NAME + " VARCHAR(255), "
            + COMPANY_TYPE + " VARCHAR(255), "
            + POSITION_IN_COMPANY+ " VARCHAR(255), "
            + COMPANY_ADDRESS + " VARCHAR(255), "
            + ZIPCODE + " VARCHAR(255), "
            + CITY + " VARCHAR(255), "
            + COMPANY_KVK+ " VARCHAR(255), "
            + VAT_NUMBER+ " INTEGER, "
            + BANK_ACCOUNT + " VARCHAR(255), "
            + LANGUAGES + " VARCHAR(255), "
            + AVAILABILITY+ " VARCHAR(255), "
            + GENDER + " VARCHAR(255), "
            + COACH_BIO+ " VARCHAR(255), "
            + VERIFIED + " BOOLEAN,"
            + DOC_PHOTO + " VARCHAR(255), "

            + INDIVIDUAL_COACHING + " BOOLEAN,"
            + CAREER_OUTPLACEMENT + " BOOLEAN,"
            + INTERVISION_COACHING + " BOOLEAN,"
            + TEAM_COACHING + " BOOLEAN,"
            + LEAN_COACHING + " BOOLEAN,"
            + AGILE_COACHING + " BOOLEAN,"
            + EXECUTIVE_COACHING + " BOOLEAN,"
            + HAPPINESS_AT_WORK + " BOOLEAN,"
            + PROJECT_MANAGERS + " BOOLEAN,"
            + ADVISIORS + " BOOLEAN,"
            + MBO + " BOOLEAN,"
            + HPO + " BOOLEAN,"
            + P_HBO + " BOOLEAN,"
            + ACADEMIC + " BOOLEAN,"

            + OTHER + " VARCHAR(255), "
            + ANSWER_1 + " VARCHAR(255), "
            + ANSWER_2 + " VARCHAR(255), "
            + ANSWER_3 + " VARCHAR(255), "
            + ANSWER_4 + " VARCHAR(255), "
            + ANSWER_5 + " VARCHAR(255), "
            + ANSWER_6 + " VARCHAR(255), "
            + ANSWER_7 + " VARCHAR(255), "
            + ANSWER_8 + " VARCHAR(255), "

            + SHORT_BIO + " VARCHAR(255), "
            + LONG_BIO + " VARCHAR(255), "

            + YES_NO_1 + " BOOLEAN,"
            + YES_NO_2 + " BOOLEAN,"
            + YES_NO_3 + " BOOLEAN,"

            + PHOTO + " VARCHAR(255));";

    private static final String CREATE_CLIENT = "CREATE TABLE " + TABLE_CLIENT + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FIRST_NAME + " VARCHAR(255), "
            + PREFIX + " VARCHAR(255), "
            + SURNAME + " VARCHAR(255), "
            + EMAIL + " VARCHAR(255), "
            + PASSWORD + " VARCHAR(255),"
            + PHONE_NUMBER + " VARCHAR(255),"
            + COMPANY_NAME + " VARCHAR(255), "
            + COMPANY_KVK + " VARCHAR(255),"
            + ADDRESS + " VARCHAR(255), "
            + ZIPCODE + " VARCHAR(255), "

            + PHOTO + " VARCHAR(255));";

    private static  final String CREATE_ADMIN = "CREATE TABLE " + TABLE_ADMIN + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + EMAIL + " VARCHAR(255), "
            + PASSWORD + " VARCHAR(255));";

    public myDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_Version);
    }

    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_COACH);
            db.execSQL(CREATE_CLIENT);
            db.execSQL(CREATE_ADMIN);
            SQLiteDatabase dbb = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(myDbHelper.EMAIL, "admin@hisna.nl");
            contentValues.put(myDbHelper.PASSWORD, "password");
            dbb.insert(myDbHelper.TABLE_ADMIN, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_COACH);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADMIN);
            onCreate(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void insertCoach(String first_name,
                            String prefix,
                            String surname,
                            String email,
                            String pass,
                            String phone_number) {

        SQLiteDatabase dbb = getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(myDbHelper.FIRST_NAME, first_name);
        contentValues.put(myDbHelper.PREFIX, prefix);
        contentValues.put(myDbHelper.SURNAME, surname);
        contentValues.put(myDbHelper.EMAIL, email);
        contentValues.put(myDbHelper.PASSWORD, pass);
        contentValues.put(myDbHelper.VERIFIED, "0");
        contentValues.put(myDbHelper.INDIVIDUAL_COACHING, "0");
        contentValues.put(myDbHelper.CAREER_OUTPLACEMENT, "0");
        contentValues.put(myDbHelper.INTERVISION_COACHING, "0");
        contentValues.put(myDbHelper.TEAM_COACHING, "0");
        contentValues.put(myDbHelper.LEAN_COACHING, "0");
        contentValues.put(myDbHelper.AGILE_COACHING, "0");
        contentValues.put(myDbHelper.EXECUTIVE_COACHING, "0");
        contentValues.put(myDbHelper.HAPPINESS_AT_WORK, "0");
        contentValues.put(myDbHelper.PROJECT_MANAGERS, "0");
        contentValues.put(myDbHelper.ADVISIORS, "0");
        contentValues.put(myDbHelper.MBO, "0");
        contentValues.put(myDbHelper.HPO, "0");
        contentValues.put(myDbHelper.P_HBO, "0");
        contentValues.put(myDbHelper.ACADEMIC, "0");

        contentValues.put(myDbHelper.YES_NO_1, "-1");
        contentValues.put(myDbHelper.YES_NO_2, "-1");
        contentValues.put(myDbHelper.YES_NO_3, "-1");
        contentValues.put(myDbHelper.PHONE_NUMBER, phone_number);

        dbb.insert(myDbHelper.TABLE_COACH, null, contentValues);
    }



    public void insertClient(String first_name,
                             String prefix,
                             String surname,
                             String email,
                             String phone_number,
                             String company_name,
                             String pass) {

        SQLiteDatabase dbb = getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(myDbHelper.FIRST_NAME, first_name);
        contentValues.put(myDbHelper.PREFIX, prefix);
        contentValues.put(myDbHelper.SURNAME, surname);
        contentValues.put(myDbHelper.EMAIL, email);
        contentValues.put(myDbHelper.PHONE_NUMBER, phone_number);
        contentValues.put(myDbHelper.COMPANY_NAME, company_name);
        contentValues.put(myDbHelper.PASSWORD, pass);

        dbb.insert(myDbHelper.TABLE_CLIENT, null, contentValues);
    }


    public Cursor getCoach() {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = {
                myDbHelper.ID,
                myDbHelper.FIRST_NAME,
                myDbHelper.PREFIX,
                myDbHelper.SURNAME,
                myDbHelper.EMAIL,
                myDbHelper.PASSWORD,
                myDbHelper.PHONE_NUMBER,
                myDbHelper.DATE_OF_BIRTH,
                myDbHelper.COMPANY_NAME,
                myDbHelper.POSITION_IN_COMPANY,
                myDbHelper.COMPANY_ADDRESS,
                myDbHelper.COMPANY_TYPE,
                myDbHelper.ZIPCODE,
                myDbHelper.CITY,
                myDbHelper.COMPANY_KVK,
                myDbHelper.VAT_NUMBER,
                myDbHelper.BANK_ACCOUNT,
                myDbHelper.LANGUAGES,
                myDbHelper.AVAILABILITY,
                myDbHelper.GENDER,
                myDbHelper.COACH_BIO,
                myDbHelper.VERIFIED,
                myDbHelper.DOC_PHOTO,

                myDbHelper.INDIVIDUAL_COACHING,
                myDbHelper.CAREER_OUTPLACEMENT,
                myDbHelper.INTERVISION_COACHING,
                myDbHelper.TEAM_COACHING,
                myDbHelper.LEAN_COACHING,
                myDbHelper.AGILE_COACHING,
                myDbHelper.EXECUTIVE_COACHING,
                myDbHelper.HAPPINESS_AT_WORK,
                myDbHelper.PROJECT_MANAGERS,
                myDbHelper.ADVISIORS,
                myDbHelper.MBO,
                myDbHelper.HPO,
                myDbHelper.P_HBO,
                myDbHelper.ACADEMIC,
                myDbHelper.OTHER,
                myDbHelper.ANSWER_1,
                myDbHelper.ANSWER_2,
                myDbHelper.ANSWER_3,
                myDbHelper.ANSWER_4,
                myDbHelper.ANSWER_5,
                myDbHelper.ANSWER_6,
                myDbHelper.ANSWER_7,
                myDbHelper.ANSWER_8,
                myDbHelper.SHORT_BIO,
                myDbHelper.LONG_BIO,
                myDbHelper.YES_NO_1,
                myDbHelper.YES_NO_2,
                myDbHelper.YES_NO_3,
                myDbHelper.PHOTO};
        Cursor cursor = db.query(myDbHelper.TABLE_COACH, columns, null, null, null, null, null);
        return cursor;
    }

    public Cursor getClientList() {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = {
                myDbHelper.ID,
                myDbHelper.FIRST_NAME,
                myDbHelper.PREFIX,
                myDbHelper.SURNAME,
                myDbHelper.EMAIL,
                myDbHelper.PASSWORD,
                myDbHelper.PHONE_NUMBER,
                myDbHelper.COMPANY_NAME,
                myDbHelper.COMPANY_KVK,
                myDbHelper.ADDRESS,
                myDbHelper.ZIPCODE};
        Cursor cursor = db.query(myDbHelper.TABLE_CLIENT, columns, null, null, null, null, null);
        return cursor;
    }

    public Cursor getAdmins() {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = {myDbHelper.ID, myDbHelper.EMAIL, myDbHelper.PASSWORD};
        Cursor cursor = db.query(myDbHelper.TABLE_ADMIN, columns, null, null, null, null, null);
        return cursor;
    }



    public void update(String table, int id, String newValue, String Column) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Column, newValue);
        String[] whereArgs = {Integer.toString(id)};
        db.update(table, contentValues, myDbHelper.ID + " = ?", whereArgs);
    }


    public Cursor rawQuery(String query, String[] selectionArgs){
        SQLiteDatabase db = getWritableDatabase();
        return db.rawQuery(query, selectionArgs);
    }


}
