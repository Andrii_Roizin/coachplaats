package com.example.coachplaats.Classes;

import android.os.Parcel;
import android.os.Parcelable;

public class Matching implements Parcelable {

    private int clientId;
    private int coachId;

    private Integer matchingPoints;

    public Matching(int clientId, int coachId){
       this.coachId = coachId;
       this.clientId = clientId;
       matchingPoints = 0;
    }


    protected Matching(Parcel in) {
        clientId = in.readInt();
        coachId = in.readInt();
        if (in.readByte() == 0) {
            matchingPoints = null;
        } else {
            matchingPoints = in.readInt();
        }
    }

    public static final Creator<Matching> CREATOR = new Creator<Matching>() {
        @Override
        public Matching createFromParcel(Parcel in) {
            return new Matching(in);
        }

        @Override
        public Matching[] newArray(int size) {
            return new Matching[size];
        }
    };

    public int getCoachId() {
        return coachId;
    }

    public void incrementMatchingScore(){
        matchingPoints++;
    }

    public Integer getMatchingPoints() {
        return matchingPoints;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(clientId);
        dest.writeInt(coachId);
        if (matchingPoints == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(matchingPoints);
        }
    }

    @Override
    public String toString(){
        return "Coach with id " + coachId + " mathed with clients with points : " + matchingPoints;
    }
}
