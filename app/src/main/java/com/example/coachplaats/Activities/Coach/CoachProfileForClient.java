package com.example.coachplaats.Activities.Coach;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coachplaats.Activities.Client.ClientProfile;
import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;


public class CoachProfileForClient extends AppCompatActivity {

    private int coachId;
    private myDbHelper dbHelper;
    private Intent coach;
    private Button cancelButton;
    private Button chooseButton;
    private CheckBox verified;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_profile_for_client);

        TextView first_name = findViewById(R.id.coach_first_name1);
        TextView city = findViewById(R.id.city1);
        TextView gender = findViewById(R.id.coach_gender1);
        TextView availability = findViewById(R.id.coach_availability1);
        ImageView coach_photo = findViewById(R.id.photo_image);

        cancelButton = findViewById(R.id.cancel_button);
        chooseButton = findViewById(R.id.choose_button);

        verified = findViewById(R.id.verified_coach);

        dbHelper = new myDbHelper(this);

        coach = getIntent();

        coachId = coach.getIntExtra("coach",0);

        Cursor cursor = dbHelper.getCoach();

        while (cursor.moveToNext()) {

             id = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));

            if(coachId == id){

                if(cursor.getInt(cursor.getColumnIndex(myDbHelper.VERIFIED)) == 1){
                    System.out.println("equals 1");
                    verified.setChecked(true);
                }else{
                    System.out.println("equals 0");
                    verified.setChecked(false);
                }

                verified.setClickable(false);

                first_name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.FIRST_NAME)));
                city.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.CITY)));
                gender.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.GENDER)));
                availability.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.AVAILABILITY)));

                String path = cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO));

                if(path == null){
                    coach_photo.setImageResource(R.drawable.photo_not_avaliable);
                }else{
                    coach_photo.setImageBitmap(BitmapFactory.decodeFile(cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO))));
                }

            }
        }

        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(CoachProfileForClient.this, "Coach was selected", Toast.LENGTH_LONG).show();
                Intent clientProfile = new Intent(CoachProfileForClient.this, ClientProfile.class);
                clientProfile.putExtra("client",id);
                startActivity(clientProfile);
            }
        });

    }
}
