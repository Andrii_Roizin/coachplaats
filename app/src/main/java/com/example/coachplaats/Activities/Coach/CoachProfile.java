package com.example.coachplaats.Activities.Coach;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coachplaats.Activities.Main.Login;
import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;


public class CoachProfile extends AppCompatActivity {

    private int coachId;
    private CheckBox verified_checkbox;
    private Button logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_profile);

         myDbHelper dbAdapter = new myDbHelper(this);

        TextView first_name = findViewById(R.id.coach_first_name);
        TextView prefix = findViewById(R.id.prefix);
        TextView surname = findViewById(R.id.surname);
        TextView email = findViewById(R.id.email);
        TextView phone_number = findViewById(R.id.phone_number);
        TextView date_of_birth = findViewById(R.id.date_of_birth);
        TextView company_name = findViewById(R.id.company_name);
        TextView position_in_company = findViewById(R.id.position_in_company);
        TextView company_address = findViewById(R.id.company_address);
        TextView zip = findViewById(R.id.zip_code);
        TextView city = findViewById(R.id.city);
        TextView company_kvk = findViewById(R.id.company_kvk);
        TextView vat_number = findViewById(R.id.vat_number);
        TextView bank_account_number = findViewById(R.id.bank_account_number);
        TextView languages = findViewById(R.id.languages);
        TextView availability = findViewById(R.id.availability);
        TextView company_type = findViewById(R.id.company_type);
        TextView gender = findViewById(R.id.gender);
        TextView coach_info = findViewById(R.id.coach_info);

        ImageView coach_photo = findViewById(R.id.photo_image);

        verified_checkbox = findViewById(R.id.verified_coach);
        logout = findViewById(R.id.logout);

        Intent intent = getIntent();

        coachId = intent.getIntExtra("coach",0);

        Cursor cursor = dbAdapter.getCoach();

        while (cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));

            if(coachId == id){

                if(cursor.getInt(cursor.getColumnIndex(myDbHelper.VERIFIED)) == 1){
                    System.out.println("equals 1");
                    verified_checkbox.setChecked(true);
                }else{
                    System.out.println("equals 0");
                    verified_checkbox.setChecked(false);
                }
                verified_checkbox.setClickable(false);

                first_name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.FIRST_NAME)));
                prefix.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.PREFIX)));
                surname.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.SURNAME)));
                email.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.EMAIL)));
                phone_number.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.PHONE_NUMBER)));
                date_of_birth.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.DATE_OF_BIRTH)));
                company_name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_NAME)));
                position_in_company.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.POSITION_IN_COMPANY)));
                company_address.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_ADDRESS)));
                zip.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ZIPCODE)));
                city.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.CITY)));
                company_kvk.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_KVK)));
                vat_number.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.VAT_NUMBER)));
                bank_account_number.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.BANK_ACCOUNT)));
                languages.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.LANGUAGES)));
                availability.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.AVAILABILITY)));

                company_type.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_TYPE)));
                gender.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.GENDER)));

                coach_info.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COACH_BIO)));
                coach_photo.setImageBitmap(BitmapFactory.decodeFile(cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO))));

                String path = cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO));

                if(path == null){
                    System.out.println("Path is null in saving in coach profile");
                    coach_photo.setImageResource(R.drawable.photo_not_avaliable);
                }else{
                    System.out.println("Path in coach profile aftere "+cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO)));
                    coach_photo.setImageBitmap(BitmapFactory.decodeFile(cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO))));
                }

            }
        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login = new Intent(CoachProfile.this, Login.class);
                startActivity(login);
            }
        });

        TextView tv_Editcoach = findViewById(R.id.change_information);
        tv_Editcoach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CoachProfile.this, CoachEdit.class);
                intent1.putExtra("coach", coachId);
                startActivity(intent1);
            }
        });

        Button td_UploadDoc = findViewById(R.id.add_document_button);
        td_UploadDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent upload_doc = new Intent(CoachProfile.this, CoachPhotoVerification.class);
                upload_doc.putExtra("coach",coachId);
                startActivity(upload_doc);
            }
        });

    }

    public void AddExtraInfo(View view) {
        Intent intent1 = new Intent(CoachProfile.this, CoachExtraInfo.class);
        intent1.putExtra("coach", coachId);
        startActivity(intent1);
    }

    public void EditProfile(View view) {
        Intent intent1 = new Intent(CoachProfile.this, CoachEdit.class);
        intent1.putExtra("coach", coachId);
        startActivity(intent1);
    }

}


