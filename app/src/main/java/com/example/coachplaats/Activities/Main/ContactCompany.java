package com.example.coachplaats.Activities.Main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.coachplaats.Activities.Client.ClientProfile;
import com.example.coachplaats.Classes.Adapters.myDbHelper;
import com.example.coachplaats.R;

public class ContactCompany extends AppCompatActivity {


    private Button cancel;
    private int id;
    private int clientId;
    private Cursor cursor;
    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_company);

        myDbHelper = new myDbHelper(this);
        cancel = findViewById(R.id.question_cancel_button);
        Intent intent = getIntent();
        clientId = intent.getIntExtra("client",0);

        cursor = myDbHelper.getClientList();

        while(cursor.moveToNext()){
            id = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));
        }



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goBack = new Intent(ContactCompany.this, ClientProfile.class);
                goBack.putExtra("client",id);
                startActivity(goBack);
            }
        });

    }
}
