package com.example.coachplaats.Activities.Coach;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coachplaats.Activities.Main.Login;
import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;

public class CreateCoach extends AppCompatActivity {

    private EditText firstnameText;
    private EditText prefixText;
    private EditText surnameText;
    private EditText emailAddressText;
    private EditText phoneNumberText;
    private EditText passwordText;
    private EditText passwordAgainText;
    private CheckBox checkBox_first;
    private CheckBox checkBox_second;
    private Button create;
    private TextView errorText;
    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_coach);

        firstnameText = findViewById(R.id.coachFirstName);
        prefixText = findViewById(R.id.coachPrefix);
        surnameText = findViewById(R.id.coachSurname);
        emailAddressText = findViewById(R.id.coachEmailAddress);
        phoneNumberText = findViewById(R.id.coachPhoneNumber);
        passwordText = findViewById(R.id.coachPassword);
        passwordAgainText = findViewById(R.id.coachPasswordAgain);

        checkBox_first = findViewById(R.id.checkBox1);
        checkBox_second = findViewById(R.id.checkBox2);

        checkBox_first.setPaintFlags(checkBox_first.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        checkBox_second.setPaintFlags(checkBox_second.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        create = findViewById(R.id.createCoachButton);
        errorText = findViewById(R.id.errorText);
        myDbHelper = new myDbHelper(this);


        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String first_name = firstnameText.getText().toString();
                String prefix = prefixText.getText().toString();
                String surname = surnameText.getText().toString();
                String email = emailAddressText.getText().toString();
                String phone_number = phoneNumberText.getText().toString();
                String password = passwordText.getText().toString();
                String passwordAgain = passwordAgainText.getText().toString();


                if (first_name.equals("")) {
                    errorText.setText("Please, type in your first name");
                }else if (surname.equals("")) {
                    errorText.setText("Please, type in your surname");
                } else if (email.equals("")) {
                    errorText.setText("Please, type in your email");
                } else if (phone_number.equals("")) {
                    errorText.setText("Please, type in your phone_number");
                }else if (password.equals("")) {
                    errorText.setText("Please, type in your password");
                }else if (passwordAgain.equals("")) {
                    errorText.setText("Please, type in your password again");
                }else if (!password.equals(passwordAgain)) {
                    errorText.setText("Repeat your password correctly,please");
                }else if (!checkBox_first.isChecked()) {
                    errorText.setText("confirm first check box");
                }else if (!checkBox_second.isChecked()) {
                    errorText.setText("confirm second check box");
                } else {
                    myDbHelper.insertCoach(first_name,prefix,surname,email,password,phone_number);
                    Intent intent = new Intent(CreateCoach.this, Login.class);
                    startActivity(intent);
                }
            }
        });
    }
}