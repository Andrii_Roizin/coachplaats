package com.example.coachplaats.Activities.Coach;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;

public class CoachQuestions extends AppCompatActivity {

    private int coachId;

    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;

    private Button cancel;
    private Button save;
    private CheckBox individual_coaching;
    private CheckBox outplacement_coaching;
    private CheckBox intervision_coaching;
    private CheckBox team_coaching;
    private CheckBox LEAN_coaching;
    private CheckBox agile_coaching;
    private CheckBox executive_coaching;
    private CheckBox happiness_coaching;
    private CheckBox project_leaders;
    private CheckBox advisor_coaching;
    private CheckBox MBO;
    private CheckBox HBO;
    private CheckBox PHBO;
    private CheckBox Academic;
    private EditText et_other;
    private EditText et_question_1;
    private EditText et_question_2;
    private EditText et_question_3;
    private EditText et_question_4;
    private EditText et_question_5;
    private EditText et_question_6;
    private EditText et_question_7;
    private EditText et_question_8;
    private EditText et_short_bio;
    private EditText et_long_bio;
    private RadioButton yes_question_1;
    private RadioButton no_question_1;
    private RadioButton yes_question_2;
    private RadioButton no_question_2;
    private RadioButton yes_question_3;
    private RadioButton no_question_3;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_questions);

        cancel = findViewById(R.id.question_cancel_button);
        save = findViewById(R.id.save_button);

        individual_coaching = findViewById(R.id.checkBox_individual_coaching);
        outplacement_coaching = findViewById(R.id.checkBox_outplacement_coaching);
        intervision_coaching = findViewById(R.id.checkBox_intervision_coaching);
        team_coaching = findViewById(R.id.checkBox_team_coaching);
        LEAN_coaching = findViewById(R.id.checkBox_LEAN_coaching);
        agile_coaching = findViewById(R.id.checkBox_agile_coaching);
        executive_coaching = findViewById(R.id.checkBox_executive_coaching);
        happiness_coaching = findViewById(R.id.checkBox_happiness_coaching);
        project_leaders = findViewById(R.id.checkBox_project_leaders);
        advisor_coaching = findViewById(R.id.checkBox_advisor_coaching);
        MBO = findViewById(R.id.checkBox_MBO);
        HBO = findViewById(R.id.checkBox_HBO);
        PHBO = findViewById(R.id.checkBox_PHBO);
        Academic = findViewById(R.id.checkBox_Academic);

        et_other = findViewById(R.id.coach_education_level);

        et_question_1 = findViewById(R.id.question3_ans);
        et_question_2 = findViewById(R.id.question4_ans);
        et_question_3 = findViewById(R.id.question5_ans);
        et_question_4 = findViewById(R.id.question6_ans);
        et_question_5 = findViewById(R.id.question7_ans);
        et_question_6 = findViewById(R.id.question8_ans);
        et_question_7 = findViewById(R.id.question9_ans);
        et_question_8 = findViewById(R.id.question10_ans);

        et_short_bio = findViewById(R.id.ans_short_bio);
        et_long_bio = findViewById(R.id.ans_long_bio);

        yes_question_1 = findViewById(R.id.radioButton);
        no_question_1 = findViewById(R.id.radioButton2);

        yes_question_2 = findViewById(R.id.radioButton3);
        no_question_2 = findViewById(R.id.radioButton4);

        yes_question_3 = findViewById(R.id.radioButton6);
        no_question_3 = findViewById(R.id.radioButton5);

        myDbHelper = new myDbHelper(this);

        Intent intent = getIntent();

        coachId = intent.getIntExtra("coach", 0);

        cursor = myDbHelper.getCoach();

        while (cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));

            if (coachId == id) {

                et_other.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.OTHER)));
                et_question_1.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ANSWER_1)));
                et_question_2.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ANSWER_2)));
                et_question_3.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ANSWER_3)));
                et_question_4.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ANSWER_4)));
                et_question_5.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ANSWER_5)));
                et_question_6.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ANSWER_6)));
                et_question_7.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ANSWER_7)));
                et_question_8.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ANSWER_8)));
                et_short_bio.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.SHORT_BIO)));
                et_long_bio.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.LONG_BIO)));

                setStatus(cursor.getColumnIndex(myDbHelper.INDIVIDUAL_COACHING),individual_coaching);
                setStatus(cursor.getColumnIndex(myDbHelper.CAREER_OUTPLACEMENT),outplacement_coaching);
                setStatus(cursor.getColumnIndex(myDbHelper.INTERVISION_COACHING),intervision_coaching);
                setStatus(cursor.getColumnIndex(myDbHelper.TEAM_COACHING),team_coaching);
                setStatus(cursor.getColumnIndex(myDbHelper.LEAN_COACHING),LEAN_coaching);
                setStatus(cursor.getColumnIndex(myDbHelper.AGILE_COACHING),agile_coaching);
                setStatus(cursor.getColumnIndex(myDbHelper.EXECUTIVE_COACHING),executive_coaching);
                setStatus(cursor.getColumnIndex(myDbHelper.HAPPINESS_AT_WORK),happiness_coaching);
                setStatus(cursor.getColumnIndex(myDbHelper.PROJECT_MANAGERS),project_leaders);
                setStatus(cursor.getColumnIndex(myDbHelper.ADVISIORS),advisor_coaching);
                setStatus(cursor.getColumnIndex(myDbHelper.MBO),MBO);
                setStatus(cursor.getColumnIndex(myDbHelper.HPO),HBO);
                setStatus(cursor.getColumnIndex(myDbHelper.P_HBO),PHBO);
                setStatus(cursor.getColumnIndex(myDbHelper.ACADEMIC),Academic);

                if(cursor.getString(cursor.getColumnIndex(myDbHelper.YES_NO_1)).equals("0")){
                    no_question_1.setChecked(true);
                    yes_question_1.setChecked(false);
                }else{
                    no_question_1.setChecked(false);
                    yes_question_1.setChecked(true);
                }

                if(cursor.getString(cursor.getColumnIndex(myDbHelper.YES_NO_2)).equals("0")){
                    no_question_2.setChecked(true);
                    yes_question_2.setChecked(false);
                }else{
                    no_question_2.setChecked(false);
                    yes_question_2.setChecked(true);
                }

                if(cursor.getString(cursor.getColumnIndex(myDbHelper.YES_NO_3)).equals("0")){
                    no_question_3.setChecked(true);
                    yes_question_3.setChecked(false);
                }else{
                    no_question_3.setChecked(false);
                    yes_question_3.setChecked(true);
                }
            }
        }


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(CoachQuestions.this, CoachExtraInfo.class);
                back.putExtra("coach",coachId);
                startActivity(back);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String other = et_other.getText().toString();
                String question_1 = et_question_1.getText().toString();
                String question_2 = et_question_2.getText().toString();
                String question_3 = et_question_3.getText().toString();
                String question_4 = et_question_4.getText().toString();
                String question_5 = et_question_5.getText().toString();
                String question_6 = et_question_6.getText().toString();
                String question_7 = et_question_7.getText().toString();
                String question_8 = et_question_8.getText().toString();
                String short_bio = et_short_bio.getText().toString();
                String long_bio = et_long_bio.getText().toString();

                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, other, com.example.coachplaats.Classes.Adapters.myDbHelper.OTHER);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, question_1, com.example.coachplaats.Classes.Adapters.myDbHelper.ANSWER_1);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, question_2, com.example.coachplaats.Classes.Adapters.myDbHelper.ANSWER_2);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, question_3, com.example.coachplaats.Classes.Adapters.myDbHelper.ANSWER_3);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, question_4, com.example.coachplaats.Classes.Adapters.myDbHelper.ANSWER_4);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, question_5, com.example.coachplaats.Classes.Adapters.myDbHelper.ANSWER_5);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, question_6, com.example.coachplaats.Classes.Adapters.myDbHelper.ANSWER_6);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, question_7, com.example.coachplaats.Classes.Adapters.myDbHelper.ANSWER_7);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, question_8, com.example.coachplaats.Classes.Adapters.myDbHelper.ANSWER_8);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, short_bio, com.example.coachplaats.Classes.Adapters.myDbHelper.SHORT_BIO);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, long_bio, com.example.coachplaats.Classes.Adapters.myDbHelper.LONG_BIO);

                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.INDIVIDUAL_COACHING,individual_coaching);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.CAREER_OUTPLACEMENT,outplacement_coaching);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.INTERVISION_COACHING,intervision_coaching);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.TEAM_COACHING,team_coaching);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.LEAN_COACHING,LEAN_coaching);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.AGILE_COACHING,agile_coaching);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.EXECUTIVE_COACHING,executive_coaching);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.HAPPINESS_AT_WORK,happiness_coaching);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.PROJECT_MANAGERS,project_leaders);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.ADVISIORS,advisor_coaching);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.MBO,MBO);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.HPO,HBO);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.P_HBO,PHBO);
                changeCheckBox(com.example.coachplaats.Classes.Adapters.myDbHelper.ACADEMIC,Academic);

                if(yes_question_1.isChecked()){
                    myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, "1", myDbHelper.YES_NO_1);
                }else{
                    myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, "0", myDbHelper.YES_NO_1);
                }

                if(yes_question_2.isChecked()){
                    myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, "1", myDbHelper.YES_NO_2);
                }else{
                    myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, "0", myDbHelper.YES_NO_2);
                }

                if(yes_question_3.isChecked()){
                    myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, "1", myDbHelper.YES_NO_3);
                }else{
                    myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, "0", myDbHelper.YES_NO_3);
                }

                Intent coachProfile = new Intent(CoachQuestions.this, CoachProfile.class);
                coachProfile.putExtra("coach",coachId);
                startActivity(coachProfile);
            }
        });


    }

    public void setStatus(int column, CheckBox checkBox){
        if(cursor.getString(column).equals("0")){
            checkBox.setChecked(false);
        }else{
            checkBox.setChecked(true);
        }
    }

    public void changeCheckBox(String column, CheckBox checkBox){
        if(checkBox.isChecked()){
            myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, "1", column);
        }else{
            myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, "0", column);
        }
    }
}
