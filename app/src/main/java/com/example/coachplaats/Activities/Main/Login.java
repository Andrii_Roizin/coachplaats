package com.example.coachplaats.Activities.Main;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coachplaats.Activities.Client.ClientProfile;
import com.example.coachplaats.Activities.Client.CreateClient;
import com.example.coachplaats.Activities.Coach.CoachProfile;
import com.example.coachplaats.Activities.Coach.CreateCoach;
import com.example.coachplaats.Classes.Adapters.myDbHelper;
import com.example.coachplaats.R;

public class Login extends AppCompatActivity {

    private TextView username_Tv;
    private TextView password_Tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username_Tv = findViewById(R.id.username_field);
        password_Tv = findViewById(R.id.clientPassword);

        TextView createCoachAccount = findViewById(R.id.create_coach);
        createCoachAccount.setPaintFlags(createCoachAccount.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        TextView createMember = findViewById(R.id.create_member);
        createMember.setPaintFlags(createMember.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        createCoachAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, CreateCoach.class);
                startActivity(intent);
            }
        });

        createMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, CreateClient.class);
                startActivity(intent);
            }
        });
    }

    public void onClientLogin(android.view.View view) {
        myDbHelper adapter = new myDbHelper(this);

        String email = username_Tv.getText().toString();
        String password = password_Tv.getText().toString();

        Cursor cursor = adapter.getClientList();

        while (cursor.moveToNext()) {

            if (cursor.getString(cursor.getColumnIndex(myDbHelper.EMAIL)).equals(email) &&
                    cursor.getString(cursor.getColumnIndex(myDbHelper.PASSWORD)).equals(password)) {


                Intent loggedIn = new Intent(Login.this, ClientProfile.class);
                loggedIn.putExtra("client", cursor.getInt(cursor.getColumnIndex(myDbHelper.ID)));
                startActivity(loggedIn);

            }
        }
    }

    public void onCoachLogin(android.view.View view) {
        myDbHelper adapter = new myDbHelper(this);

        String email = username_Tv.getText().toString();
        String password = password_Tv.getText().toString();

        Cursor cursor = adapter.getCoach();
        while (cursor.moveToNext()) {
            if (cursor.getString(cursor.getColumnIndex(myDbHelper.EMAIL)).equals(email) &&
                    cursor.getString(cursor.getColumnIndex(myDbHelper.PASSWORD)).equals(password)) {

                Intent loggedIn = new Intent(Login.this, CoachProfile.class);
                loggedIn.putExtra("coach", cursor.getInt(cursor.getColumnIndex(myDbHelper.ID)));
                startActivity(loggedIn);
            }
        }
    }

    public void onAdminLogin(android.view.View view) {
        myDbHelper adapter = new myDbHelper(this);

        String email = username_Tv.getText().toString();
        String password = password_Tv.getText().toString();

        Cursor cursor = adapter.getAdmins();
        while (cursor.moveToNext()) {
            if (cursor.getString(cursor.getColumnIndex(myDbHelper.EMAIL)).equals(email) &&
                    cursor.getString(cursor.getColumnIndex(myDbHelper.PASSWORD)).equals(password)) {
                Intent loggedIn = new Intent(Login.this, MatchingResults.class);
                loggedIn.putExtra("admin", cursor.getInt(cursor.getColumnIndex(myDbHelper.ID)));
                startActivity(loggedIn);
            }
        }
    }
}