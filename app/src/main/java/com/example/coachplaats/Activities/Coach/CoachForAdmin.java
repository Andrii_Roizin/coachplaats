package com.example.coachplaats.Activities.Coach;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.coachplaats.Activities.Main.MatchingResults;
import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;

public class CoachForAdmin extends AppCompatActivity {

    private TextView name;
    private Button verify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_from_admin);

        ImageView document = findViewById(R.id.CoachDocumentIV);
        name = findViewById(R.id.coach_name);
        verify = findViewById(R.id.verify_coach);


        Intent intent = getIntent();
        myDbHelper myDbHelper = new myDbHelper(this);
        int coachId = intent.getIntExtra("coach", 0);
        int adminId= intent.getIntExtra("admin", 0);
        Cursor cursor = myDbHelper.getCoach();

        while(cursor.moveToNext()){
            if (cursor.getInt(cursor.getColumnIndex(myDbHelper.ID)) == coachId) {


                name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.FIRST_NAME)));

                if (cursor.getString(cursor.getColumnIndex(myDbHelper.DOC_PHOTO)) != null) {

                    document.setImageBitmap(BitmapFactory.decodeFile(cursor.getString(cursor.getColumnIndex(myDbHelper.DOC_PHOTO))));
                }
            }
        }

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDbHelper.update(myDbHelper.TABLE_COACH,coachId,"1",myDbHelper.VERIFIED);
                Intent coachList = new Intent(CoachForAdmin.this, MatchingResults.class);
                coachList.putExtra("admin",adminId);
                startActivity(coachList);
                finish();
            }
        });

    }
}
