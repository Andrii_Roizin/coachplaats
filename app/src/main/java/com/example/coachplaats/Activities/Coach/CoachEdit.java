package com.example.coachplaats.Activities.Coach;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;


public class CoachEdit extends AppCompatActivity {

    private int coachId;
    private int photoId;
    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;
    private Uri pickedImage;
    private Bitmap photo;
    private ImageView coachPhoto;
    private String photoString;
    private String path;
    private File mypath;
    private String photoSavedPath;
    private EditText et_First_name;
    private EditText et_Prefix;
    private EditText et_Surname;
    private EditText et_Email;
    private EditText et_Phone_number;
    private EditText et_Date_of_birth;
    private EditText et_Company_name;
    private EditText et_Position_in_company;
    private EditText et_Company_address;
    private EditText et_Zip_code;
    private EditText et_City;
    private EditText et_Company_kvk;
    private EditText et_Van_number;
    private EditText et_Bank_account_number;
    private EditText et_Languages;
    private EditText et_Availability;
    private EditText et_coachInfo;
    private EditText et_Company_type;
    private EditText et_Gender;


    private int CAMERA_ACTIVITY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_edit);
        myDbHelper = new myDbHelper(this);

        photoId = 2;

        et_First_name = findViewById(R.id.coach_first_name);
        et_Prefix = findViewById(R.id.prefix);
        et_Surname = findViewById(R.id.surname);
        et_Email = findViewById(R.id.email);
        et_Phone_number = findViewById(R.id.phone_number);
        et_Date_of_birth = findViewById(R.id.date_of_birth);
        et_Company_name = findViewById(R.id.company_name);
        et_Position_in_company = findViewById(R.id.position_in_company);
        et_Company_address = findViewById(R.id.company_address);
        et_Zip_code = findViewById(R.id.zip_code);
        et_City = findViewById(R.id.city);
        et_Company_kvk = findViewById(R.id.company_kvk);
        et_Van_number = findViewById(R.id.vat_number);
        et_Bank_account_number = findViewById(R.id.bank_account_number);
        et_Languages = findViewById(R.id.languages);
        et_Availability = findViewById(R.id.availability);
        et_Company_type = findViewById(R.id.company_type);
        et_Gender = findViewById(R.id.gender);
        et_coachInfo = findViewById(R.id.coach_info);
        coachPhoto = findViewById(R.id.photo_image);

        Intent intent = getIntent();
        coachId = intent.getIntExtra("coach", 0);
        final Cursor cursor = myDbHelper.getCoach();

        while (cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));

            if (coachId == id) {

                et_First_name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.FIRST_NAME)));
                et_Prefix.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.PREFIX)));
                et_Surname.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.SURNAME)));
                et_Email.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.EMAIL)));
                et_Phone_number.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.PHONE_NUMBER)));
                et_Date_of_birth.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.DATE_OF_BIRTH)));
                et_Company_name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_NAME)));
                et_Position_in_company.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.POSITION_IN_COMPANY)));
                et_Company_address.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_ADDRESS)));
                et_Zip_code.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ZIPCODE)));
                et_City.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.CITY)));
                et_Company_kvk.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_KVK)));
                et_Van_number.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.VAT_NUMBER)));
                et_Bank_account_number.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.BANK_ACCOUNT)));
                et_Languages.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.LANGUAGES)));
                et_Availability.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.AVAILABILITY)));
                et_coachInfo.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COACH_BIO)));

                et_Company_type.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_TYPE)));
                et_Gender.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.GENDER)));

                path = cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO));

                if(path == null){
                    System.out.println("path is null");
                    coachPhoto.setImageResource(R.drawable.photo_not_avaliable);
                }else{
                    System.out.println("path is: " + cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO)));
                    coachPhoto.setImageBitmap(BitmapFactory.decodeFile(cursor.getString(cursor.getColumnIndex(myDbHelper.PHOTO))));
                }

            }
        }


        Button bt_Save = findViewById(R.id.save_button);
        bt_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String first_name = et_First_name.getText().toString();
                String prefix = et_Prefix.getText().toString();
                String surname = et_Surname.getText().toString();
                String email = et_Email.getText().toString();
                String phone_number = et_Phone_number.getText().toString();
                String date_of_birth = et_Date_of_birth.getText().toString();
                String company_name = et_Company_name.getText().toString();
                String position_in_company = et_Position_in_company.getText().toString();
                String company_address = et_Company_address.getText().toString();
                String zip = et_Zip_code.getText().toString();
                String city = et_City.getText().toString();
                String company_kvk = et_Company_kvk.getText().toString();
                String van_number = et_Van_number.getText().toString();
                String bank_account_number = et_Bank_account_number.getText().toString();
                String languages = et_Languages.getText().toString();
                String availability = et_Availability.getText().toString();

                String company_type = et_Company_type.getText().toString();
                String gender = et_Gender.getText().toString();

                String coach_info = et_coachInfo.getText().toString();

                Intent backToCoachProfile;

                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, first_name, com.example.coachplaats.Classes.Adapters.myDbHelper.FIRST_NAME);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, prefix, com.example.coachplaats.Classes.Adapters.myDbHelper.PREFIX);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, surname, com.example.coachplaats.Classes.Adapters.myDbHelper.SURNAME);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, email, com.example.coachplaats.Classes.Adapters.myDbHelper.EMAIL);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, phone_number, com.example.coachplaats.Classes.Adapters.myDbHelper.PHONE_NUMBER);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, date_of_birth, com.example.coachplaats.Classes.Adapters.myDbHelper.DATE_OF_BIRTH);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, company_name, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_NAME);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, position_in_company, com.example.coachplaats.Classes.Adapters.myDbHelper.POSITION_IN_COMPANY);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, company_address, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_ADDRESS);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, zip, com.example.coachplaats.Classes.Adapters.myDbHelper.ZIPCODE);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, city, com.example.coachplaats.Classes.Adapters.myDbHelper.CITY);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, company_kvk, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_KVK);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, van_number, com.example.coachplaats.Classes.Adapters.myDbHelper.VAT_NUMBER);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, bank_account_number, com.example.coachplaats.Classes.Adapters.myDbHelper.BANK_ACCOUNT);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, languages, com.example.coachplaats.Classes.Adapters.myDbHelper.LANGUAGES);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, availability, com.example.coachplaats.Classes.Adapters.myDbHelper.AVAILABILITY);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, coach_info, com.example.coachplaats.Classes.Adapters.myDbHelper.COACH_BIO);

                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, company_type, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_TYPE);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, gender, com.example.coachplaats.Classes.Adapters.myDbHelper.GENDER);

                backToCoachProfile = new Intent(CoachEdit.this, CoachProfile.class);

                if(path == null){
                    myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, photoSavedPath, com.example.coachplaats.Classes.Adapters.myDbHelper.PHOTO);
                    backToCoachProfile.putExtra("coach", coachId);
                    startActivity(backToCoachProfile);
                }else{

                    backToCoachProfile.putExtra("coach", coachId);
                    startActivity(backToCoachProfile);
                }
            }
        });

        Button bt_Cancel = findViewById(R.id.cancel_button);
        bt_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CoachEdit.this, CoachProfile.class);
                intent1.putExtra("coach", coachId);
                startActivity(intent1);
            }
        });

        Button bt_photo = findViewById(R.id.upload_photo_button);
        bt_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, CAMERA_ACTIVITY);
            }
        });
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("imageDirCoachPhoto", Context.MODE_PRIVATE);
        mypath = new File(directory, coachId + "image.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            Log.i("Message", "Exception");
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                Log.i("Message", "IOException");
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath()+'/' + coachId + "image.jpg";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    pickedImage = data.getData();
                    photo = MediaStore.Images.Media.getBitmap(this.getContentResolver(), pickedImage);
                    coachPhoto.setImageBitmap(photo);
                    photoSavedPath = saveToInternalStorage(photo);
                    Log.d("photo i","path is: " + photoSavedPath);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
