package com.example.coachplaats.Activities.Main;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coachplaats.Classes.Matching;
import com.example.coachplaats.Classes.Adapters.myDbHelper;
import com.example.coachplaats.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class ActivityMatching extends AppCompatActivity {

    private Intent intent;
    private int clientId;
    private EditText coachTypeText;
    private EditText coachLocationText;
    private EditText coachAvailabilityText;
    private EditText coachEducationText;
    private RadioButton genderMale;
    private RadioButton genderFemale;
    private Button search;
    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper = new myDbHelper(this);
    private ArrayList<Matching> matchingResults;
    private String coachType;
    private String coachLocation;
    private Integer coachAvailability;
    private String coachEducation;
    private String coachGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matching);

        coachTypeText = findViewById(R.id.coach_gender);
        coachAvailabilityText = findViewById(R.id.coach_availability);
        coachLocationText = findViewById(R.id.coach_city);
        coachEducationText = findViewById(R.id.education_education_level);

        genderMale = findViewById(R.id.radio_male);
        genderFemale = findViewById(R.id.radio_female);

        search = findViewById(R.id.button_search);
        matchingResults = new ArrayList<>();

        intent = getIntent();
        clientId = intent.getIntExtra("client", -1);

        genderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coachGender = "male";
            }
        });

        genderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coachGender = "female";
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coachType = coachTypeText.getText().toString();
                coachAvailability = Integer.parseInt(coachAvailabilityText.getText().toString());
                coachLocation = coachLocationText.getText().toString();
                coachEducation = coachEducationText.getText().toString();

                Cursor cursor = myDbHelper.getCoach();

                while (cursor.moveToNext()) {

                    int coachId = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));
                    String location = cursor.getString(cursor.getColumnIndex(myDbHelper.CITY));
                    String gender = cursor.getString(cursor.getColumnIndex(myDbHelper.GENDER));
                    Integer availability = cursor.getInt(cursor.getColumnIndex(myDbHelper.AVAILABILITY));


                    Matching matching = new Matching(clientId, coachId);

                    if (location != null) {
                        if (location.equals(coachLocation)) {
                            matching.incrementMatchingScore();
                        }
                    }

                    if (gender != null) {
                        if (gender.equals(coachGender)) {
                            matching.incrementMatchingScore();
                        }
                    }

                    if (availability != null) {
                        if (availability >= coachAvailability) {
                            matching.incrementMatchingScore();
                        }
                    }

                    if (matching.getMatchingPoints() > 0) {
                        matchingResults.add(matching);
                    }

                    Collections.sort(matchingResults, new Comparator<Matching>() {
                        public int compare(Matching m1, Matching m2) {

                            if (m1.getMatchingPoints() > m2.getMatchingPoints()) {
                                return 1;
                            } else {
                                if (m1.getMatchingPoints() < m2.getMatchingPoints()) {
                                    return -1;
                                } else return 0;
                            }

                        }

                    });
                }

                Intent intent = new Intent(ActivityMatching.this, MatchingResults.class);
                intent.putExtra("location", coachLocation);
                intent.putExtra("availability", coachAvailability);
                intent.putExtra("gender", coachGender);
                intent.putParcelableArrayListExtra("matchingResults", matchingResults);
                startActivity(intent);
            }
        });
    }


}
