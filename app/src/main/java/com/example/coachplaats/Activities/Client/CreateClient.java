package com.example.coachplaats.Activities.Client;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coachplaats.Activities.Main.Login;
import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;


public class CreateClient extends AppCompatActivity {

    private EditText firstNameText;
    private EditText prefixText;
    private EditText surnameText;
    private EditText emailText;
    private EditText phoneText;
    private EditText companyText;
    private EditText passwordText;
    private EditText passwordAgainText;
    private Button createClient;
    private TextView errorText;
    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;
    private CheckBox checkBox_1;
    private CheckBox checkBox_2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_client);

        firstNameText = findViewById(R.id.clientFirstName);
        prefixText = findViewById(R.id.clientPrefix);
        surnameText = findViewById(R.id.clientSurname);
        emailText = findViewById(R.id.clientEmailAddress);
        phoneText = findViewById(R.id.clientPhone);
        companyText = findViewById(R.id.companyName);
        passwordText = findViewById(R.id.clientPassword);
        passwordAgainText = findViewById(R.id.clientPasswordAgain);

        checkBox_1 = findViewById(R.id.checkBox1);
        checkBox_2 = findViewById(R.id.checkBox2);

        checkBox_1.setPaintFlags(checkBox_1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        checkBox_2.setPaintFlags(checkBox_2.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        createClient = findViewById(R.id.clientCreateButton);
        errorText = findViewById(R.id.clientError);
        myDbHelper = new myDbHelper(this);

        createClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = firstNameText.getText().toString();
                String prefix = prefixText.getText().toString();
                String surname = surnameText.getText().toString();
                String email = emailText.getText().toString();
                String phoneNumber = phoneText.getText().toString();
                String company = companyText.getText().toString();
                String password = passwordText.getText().toString();
                String passwordAgain = passwordAgainText.getText().toString();


                if (firstName.equals("")) {
                    errorText.setText("Please, type in your first name");
                } else {
                    if (surname.equals("")) {
                        errorText.setText("Please, type in your surname");
                    } else {
                        if (email.equals("")) {
                            errorText.setText("Please, type in your email");
                        } else {
                            if (phoneNumber.equals("")) {
                                errorText.setText("Please, type in your phoneText");
                            } else {
                                if (company.equals("")) {
                                    errorText.setText("Please, type in your company name");
                                } else {
                                    if (password.equals("")) {
                                        errorText.setText("Please, type in your password ");
                                    } else {
                                        if (passwordAgain.equals("")) {
                                            errorText.setText("Please, type in your password again");
                                        } else {
                                            if (!password.equals(passwordAgain)) {
                                                errorText.setText("Repeat your password correctly,please");
                                            } else if(!checkBox_1.isChecked()){
                                                errorText.setText("Please press on the first checkbox");
                                            }else if(!checkBox_1.isChecked()){
                                                errorText.setText("Please press on the secondcheckbox");
                                            } else{
                                                myDbHelper.insertClient(firstName, prefix, surname, email, phoneNumber, company, password);
                                                Intent intent = new Intent(CreateClient.this, Login.class);
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        });
    }
}