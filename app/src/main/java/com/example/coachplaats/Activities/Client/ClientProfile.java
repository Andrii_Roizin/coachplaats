package com.example.coachplaats.Activities.Client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.coachplaats.Activities.Main.ActivityMatching;
import com.example.coachplaats.Activities.Main.ContactCompany;
import com.example.coachplaats.Activities.Main.Login;
import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;

public class ClientProfile extends AppCompatActivity {

    private int clientId;
    private int id;
    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;

    private Button change_profile;
    private Button search_for_coach;
    private Button logout;
    private Button contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_profile2);

        change_profile = findViewById(R.id.button);
        search_for_coach = findViewById(R.id.button2);
        logout = findViewById(R.id.button3);
        contact = findViewById(R.id.button4);

        myDbHelper = new myDbHelper(this);

        Intent intent = getIntent();

        clientId = intent.getIntExtra("client",0);

        Cursor cursor = myDbHelper.getClientList();

        while(cursor.moveToNext()){
            id = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));
        }

        change_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeProfile = new Intent(ClientProfile.this, ClientEdit.class);
                changeProfile.putExtra("client",id);
                startActivity(changeProfile);
            }
        });

        search_for_coach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listOfCoaches = new Intent(ClientProfile.this, ActivityMatching.class);
                listOfCoaches.putExtra("client",id);
                startActivity(listOfCoaches);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login = new Intent(ClientProfile.this, Login.class);
                startActivity(login);
            }
        });

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contact = new Intent(ClientProfile.this, ContactCompany.class);
                contact.putExtra("coach",id);
                startActivity(contact);
            }
        });

    }




}
