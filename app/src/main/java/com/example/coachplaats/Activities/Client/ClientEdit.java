package com.example.coachplaats.Activities.Client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;

public class ClientEdit extends AppCompatActivity {

    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;
    private int clientId;
    private int id;
    private Button save;
    private Button cancel;
    private EditText et_first_name;
    private EditText et_prefix;
    private EditText et_surname;
    private EditText et_email;
    private EditText et_phone_number;
    private EditText et_company_name;
    private EditText et_company_kvk;
    private EditText et_address;
    private EditText et_zipcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_edit);
        myDbHelper = new myDbHelper(this);

        et_first_name = findViewById(R.id.client_first_name);
        et_prefix = findViewById(R.id.client_prefix);
        et_surname = findViewById(R.id.client_surname);
        et_email = findViewById(R.id.client_email);
        et_phone_number = findViewById(R.id.client_phone_number);
        et_company_name = findViewById(R.id.client_company_name);
        et_company_kvk = findViewById(R.id.client_company_kvk);
        et_address = findViewById(R.id.client_address);
        et_zipcode = findViewById(R.id.client_zipcode);

        save = findViewById(R.id.save_button);
        cancel = findViewById(R.id.cancel_button);


        Intent intent = getIntent();

        clientId = intent.getIntExtra("client",0);

        Cursor cursor = myDbHelper.getClientList();

        while (cursor.moveToNext()) {

            id = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));

            if(clientId == id){

                et_first_name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.FIRST_NAME)));
                et_prefix.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.PREFIX)));
                et_surname.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.SURNAME)));
                et_email.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.EMAIL)));
                et_phone_number.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.PHONE_NUMBER)));
                et_company_name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_NAME)));
                et_company_kvk.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_KVK)));
                et_address.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ADDRESS)));
                et_zipcode.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ZIPCODE)));

            }
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String first_name = et_first_name.getText().toString();
                String prefix = et_prefix.getText().toString();
                String surname = et_surname.getText().toString();
                String email = et_email.getText().toString();
                String phone_number = et_phone_number.getText().toString();
                String company_name = et_company_name.getText().toString();
                String company_kvk = et_company_kvk.getText().toString();
                String address = et_address.getText().toString();
                String zipcodee= et_zipcode.getText().toString();

                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_CLIENT, clientId, first_name, com.example.coachplaats.Classes.Adapters.myDbHelper.FIRST_NAME);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_CLIENT, clientId, prefix, com.example.coachplaats.Classes.Adapters.myDbHelper.PREFIX);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_CLIENT, clientId, surname, com.example.coachplaats.Classes.Adapters.myDbHelper.SURNAME);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_CLIENT, clientId, email, com.example.coachplaats.Classes.Adapters.myDbHelper.EMAIL);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_CLIENT, clientId, phone_number, com.example.coachplaats.Classes.Adapters.myDbHelper.PHONE_NUMBER);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_CLIENT, clientId, company_name, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_NAME);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_CLIENT, clientId, company_kvk, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_KVK);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_CLIENT, clientId, address, com.example.coachplaats.Classes.Adapters.myDbHelper.ADDRESS);
                myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_CLIENT, clientId, zipcodee, com.example.coachplaats.Classes.Adapters.myDbHelper.ZIPCODE);

                Intent save = new Intent(ClientEdit.this, ClientProfile.class);
                save.putExtra("client",clientId);
                startActivity(save);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cancel = new Intent(ClientEdit.this, ClientProfile.class);
                cancel.putExtra("client",clientId);
                startActivity(cancel);
            }
        });

    }
}
