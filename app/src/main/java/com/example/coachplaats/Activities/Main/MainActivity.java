package com.example.coachplaats.Activities.Main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coachplaats.Classes.Adapters.myDbHelper;
import com.example.coachplaats.R;

public class MainActivity extends AppCompatActivity {
    private Button startButton;

    myDbHelper db = new myDbHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startButton = findViewById(R.id.startButton);
        goToLogin();

//        myDbHelper myDbHelper = new myDbHelper(this);
//        myDbHelper.onUpgrade(myDbHelper.getWritableDatabase(),1, 2);
    }


    public void goToLogin(){
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
            }
        });
    }

    public void goToCoachesList(){
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MatchingResults.class);
                startActivity(intent);
            }
        });
    }
}

