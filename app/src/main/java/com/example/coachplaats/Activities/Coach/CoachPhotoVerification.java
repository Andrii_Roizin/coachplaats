package com.example.coachplaats.Activities.Coach;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class CoachPhotoVerification extends AppCompatActivity {

    private Button upload_photo;
    private Button save_photo;
    private ImageView doc_photo;
    private TextView coach_name;
    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;
    private int coachId;
    private File mypath;
    private Uri pickedImage;
    private Bitmap photo;
    private String photoString;
    private String path;
    private String pathDocPhoto;
    private int CAMERA_ACTIVITY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_photo_verification);

        upload_photo = findViewById(R.id.button_upload_doc);
        save_photo = findViewById(R.id.button_save_doc);
        doc_photo = findViewById(R.id.doc_photo);
        coach_name = findViewById(R.id.coach_name);
        myDbHelper = new myDbHelper(this);

        Intent intent = getIntent();
        coachId = intent.getIntExtra("coach", 0);
        final Cursor cursor = myDbHelper.getCoach();

        while (cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));

            if (coachId == id) {

                path = cursor.getString(cursor.getColumnIndex(myDbHelper.DOC_PHOTO));
                coach_name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.FIRST_NAME)));
                if (path == null) {
                    doc_photo.setImageResource(R.drawable.document);
                } else {
                    doc_photo.setImageBitmap(BitmapFactory.decodeFile(cursor.getString(cursor.getColumnIndex(myDbHelper.DOC_PHOTO))));
                }

            }
        }

        save_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent coach_profile;

                if (path == null) {
                    myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, pathDocPhoto, com.example.coachplaats.Classes.Adapters.myDbHelper.DOC_PHOTO);
                    coach_profile = new Intent(CoachPhotoVerification.this, CoachProfile.class);
                    coach_profile.putExtra("coach", coachId);
                    startActivity(coach_profile);
                } else {
                    coach_profile = new Intent(CoachPhotoVerification.this, CoachProfile.class);
                    coach_profile.putExtra("coach", coachId);
                    startActivity(coach_profile);
                }
            }
        });

        upload_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, CAMERA_ACTIVITY);
            }
        });
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("imageDirDocPhoto", Context.MODE_PRIVATE);
        mypath = new File(directory, coachId + "image.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            Log.i("Message", "Exception");
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                Log.i("Message", "IOException");
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath()+'/' + coachId + "image.jpg";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    pickedImage = data.getData();
                    photo = MediaStore.Images.Media.getBitmap(this.getContentResolver(), pickedImage);
                    doc_photo.setImageBitmap(photo);
                    pathDocPhoto = saveToInternalStorage(photo);
                    Log.i("Photo", "path is:" + photoString);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }


}
