package com.example.coachplaats.Activities.Main;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FilterQueryProvider;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coachplaats.Activities.Coach.CoachForAdmin;
import com.example.coachplaats.Activities.Coach.CoachProfileForClient;
import com.example.coachplaats.Classes.Matching;
import com.example.coachplaats.Classes.Adapters.listAdapter;
import com.example.coachplaats.Classes.Adapters.myDbHelper;
import com.example.coachplaats.R;

import java.util.ArrayList;

public class MatchingResults extends AppCompatActivity {

    com.example.coachplaats.Classes.Adapters.listAdapter listAdapter;

    private Cursor cursor;
    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;
    private ArrayList<Matching> matchingResults;
    private String location;
    private int availability;
    private String gender;
    private ListView coaches;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matching_results);

        myDbHelper = new myDbHelper(this);

        cursor = myDbHelper.getCoach();

        Intent intent = getIntent();
        int adminId = intent.getIntExtra("admin", 0);

        listAdapter = new listAdapter(MatchingResults.this, cursor, matchingResults);
        coaches = findViewById(R.id.coach_list);

        if (adminId == 0) {
            matchingResults = intent.getParcelableArrayListExtra("matchingResults");
            location = intent.getStringExtra("location");
            availability = intent.getIntExtra("availability", 0);
            gender = intent.getStringExtra("gender");


            System.out.println(matchingResults);

            listAdapter.setFilterQueryProvider(new FilterQueryProvider() {
                @Override
                public Cursor runQuery(CharSequence constraint) {
                    String query = "SELECT _id, Name, DATE_OF_BIRTH, CITY, AVAILABILITY, VERIFIED, PHOTO, GENDER FROM Coach WHERE CITY = ? AND AVAILABILITY >= ? AND GENDER = ?";
                    String[] arr = constraint.toString().split(";");
                    return myDbHelper.rawQuery(query, arr);
                }
            });

            String matching = location + ";" + availability + ";" + gender;

            listAdapter.getFilter().filter(matching);

            coaches.setAdapter(listAdapter);

            coaches.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    Intent coachProfile = new Intent(MatchingResults.this, CoachProfileForClient.class);
                    Cursor coach = (Cursor) listAdapter.getItem(position);
                    coachProfile.putExtra("coach", coach.getInt(coach.getColumnIndexOrThrow(myDbHelper.ID)));
                    startActivity(coachProfile);
                    finish();
                }
            });
        } else {

            coaches.setAdapter(listAdapter);

            coaches.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    Intent coachProfile = new Intent(MatchingResults.this, CoachForAdmin.class);
                    Cursor coach = (Cursor) listAdapter.getItem(position);
                    coachProfile.putExtra("coach", coach.getInt(coach.getColumnIndexOrThrow(myDbHelper.ID)));
                    coachProfile.putExtra("admin", adminId);
                    startActivity(coachProfile);
                }
            });

        }
    }
}
