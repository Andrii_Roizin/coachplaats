package com.example.coachplaats.Activities.Coach;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.coachplaats.R;
import com.example.coachplaats.Classes.Adapters.myDbHelper;

public class CoachExtraInfo extends AppCompatActivity {

    private int coachId;

    private com.example.coachplaats.Classes.Adapters.myDbHelper myDbHelper;

    private EditText et_gender;
    private EditText et_birthday;
    private EditText et_company_name;
    private EditText et_company_type;
    private EditText et_address;
    private EditText et_zip_code;
    private EditText et_location;
    private EditText et_kvk_number;
    private EditText et_vat_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_extra_info);

        myDbHelper = new myDbHelper(this);

        et_gender = findViewById(R.id.coach_gender);
        et_birthday = findViewById(R.id.coach_birthday);
        et_company_name = findViewById(R.id.coach_company_name);
        et_company_type = findViewById(R.id.coach_company_type);
        et_address = findViewById(R.id.coach_address);
        et_zip_code = findViewById(R.id.coach_zip_code);
        et_location = findViewById(R.id.coach_location);
        et_kvk_number = findViewById(R.id.coach_kvk_nr);
        et_vat_number = findViewById(R.id.coach_VAT_nr);


        Intent intent = getIntent();

        coachId = intent.getIntExtra("coach", 0);

        final Cursor cursor = myDbHelper.getCoach();

        while (cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(myDbHelper.ID));

            if (coachId == id) {

                et_gender.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.GENDER)));
                et_birthday.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.DATE_OF_BIRTH)));
                et_company_name.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_NAME)));
                et_company_type.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_TYPE)));
                et_address.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_ADDRESS)));
                et_zip_code.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.ZIPCODE)));
                et_location.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.CITY)));
                et_kvk_number.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.COMPANY_KVK)));
                et_vat_number.setText(cursor.getString(cursor.getColumnIndex(myDbHelper.VAT_NUMBER)));

            }
        }
    }

    public void GoToQuestions(View view) {

        String gender = et_gender.getText().toString();
        String birthday = et_birthday.getText().toString();
        String company_name = et_company_name.getText().toString();
        String company_type = et_company_type.getText().toString();
        String address = et_address.getText().toString();
        String zip_codee = et_zip_code.getText().toString();
        String location = et_location.getText().toString();
        String kvk_number = et_kvk_number.getText().toString();
        String vat_number = et_vat_number.getText().toString();

        myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, gender, com.example.coachplaats.Classes.Adapters.myDbHelper.GENDER);
        myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, birthday, com.example.coachplaats.Classes.Adapters.myDbHelper.DATE_OF_BIRTH);
        myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, company_name, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_NAME);
        myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, company_type, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_TYPE);
        myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, address, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_ADDRESS);
        myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, zip_codee, com.example.coachplaats.Classes.Adapters.myDbHelper.ZIPCODE);
        myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, location, com.example.coachplaats.Classes.Adapters.myDbHelper.CITY);
        myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, kvk_number, com.example.coachplaats.Classes.Adapters.myDbHelper.COMPANY_KVK);
        myDbHelper.update(com.example.coachplaats.Classes.Adapters.myDbHelper.TABLE_COACH, coachId, vat_number, com.example.coachplaats.Classes.Adapters.myDbHelper.VAT_NUMBER);


        Intent intent1 = new Intent(CoachExtraInfo.this, CoachQuestions.class);
        intent1.putExtra("coach", coachId);
        startActivity(intent1);
    }
}
